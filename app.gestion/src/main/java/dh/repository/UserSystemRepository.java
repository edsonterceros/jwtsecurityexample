package dh.repository;

import dh.domain.UserSystem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserSystemRepository extends JpaRepository<UserSystem, Long> {

    UserSystem findByUserName(String userName);
}
