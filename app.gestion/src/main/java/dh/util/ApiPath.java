package dh.util;

public class ApiPath {
    public static final String BASE_URL = "/";

    //this path for login
    public static final String LOGIN_URL = BASE_URL + "login";

    //This are constants for User
    public static final String USER_PATH = BASE_URL + "users";

}
