package dh.model;

public class Home {

	private String message;
	
	public Home(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Home [message=" + message + "]";
	}
}
