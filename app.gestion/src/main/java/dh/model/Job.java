package dh.model;

public class Job {

	private String jobNumber;
	private Integer zipCode;
	private String address;
	private String state;
	private String country;
	private String owner;
	private String parcel;
	private String pictureURL;
	private Boolean status;
	
	public Job(String jobNumber, Integer zipCode, String address, String state, String country, String owner,
			String parcel, String pictureURL, Boolean status) {
		super();
		this.jobNumber = jobNumber;
		this.zipCode = zipCode;
		this.address = address;
		this.state = state;
		this.country = country;
		this.owner = owner;
		this.parcel = parcel;
		this.pictureURL = pictureURL;
		this.status = status;
	}
	public String getJobNumber() {
		return jobNumber;
	}
	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}
	public Integer getZipCode() {
		return zipCode;
	}
	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getParcel() {
		return parcel;
	}
	public void setParcel(String parcel) {
		this.parcel = parcel;
	}
	public String getPictureURL() {
		return pictureURL;
	}
	public void setPictureURL(String pictureURL) {
		this.pictureURL = pictureURL;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Job [jobNumber=" + jobNumber + ", zipCode=" + zipCode + ", address=" + address + ", state=" + state
				+ ", country=" + country + ", owner=" + owner + ", parcel=" + parcel + ", pictureURL=" + pictureURL
				+ ", status=" + status + "]";
	}
}

