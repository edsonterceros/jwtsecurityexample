package dh.service;

import dh.domain.UserSystem;

import java.util.List;

public interface UserSystemService {
    List<UserSystem> getAllUsers();

    void saveUser(UserSystem user);

    UserSystem findByUserName(String userName);
}
