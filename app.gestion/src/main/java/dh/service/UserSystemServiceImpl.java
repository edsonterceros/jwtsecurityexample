package dh.service;

import dh.domain.UserSystem;
import dh.repository.UserSystemRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Collections.emptyList;

@Service
public class UserSystemServiceImpl implements UserSystemService, UserDetailsService {

    private UserSystemRepository userSystemRepository;


    public UserSystemServiceImpl(UserSystemRepository userSystemRepository) {
        this.userSystemRepository = userSystemRepository;
    }

    @Override
    public List<UserSystem> getAllUsers() {
        return userSystemRepository.findAll();
    }

    @Override
    public void saveUser(UserSystem user) {
        userSystemRepository.save(user);
    }

    @Override
    public UserSystem findByUserName(String userName) {
        return userSystemRepository.findByUserName(userName);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserSystem usuario = userSystemRepository.findByUserName(username);
        if (usuario == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(usuario.getUserName(), usuario.getPassword(), emptyList());
    }
}
