package dh;

import dh.domain.Role;
import dh.domain.UserSystem;
import dh.repository.RoleRepository;
import dh.repository.UserSystemRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private UserSystemRepository userSystemRepository;
    private RoleRepository roleRepository;

    public DevBootstrap(UserSystemRepository userSystemRepository, RoleRepository roleRepository) {
        this.userSystemRepository = userSystemRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        // load initial user data
        loadDefaultUser();
    }

    private void loadDefaultUser() {
        Set<Role> listRoles = new HashSet<Role>();
        if (this.roleRepository.count() == 0) {
            Role role = new Role();
            role.setRoleName("ROLE_USER");
            role.setVersion(1);
            role.setCreatedOn(new Date());
            this.roleRepository.save(role);
            listRoles.add(role);
            Role adminRole = new Role();
            adminRole.setRoleName("ROLE_ADMIN");
            adminRole.setVersion(1);
            adminRole.setCreatedOn(new Date());
            this.roleRepository.save(adminRole);
            listRoles.add(adminRole);
        }
        if (this.userSystemRepository.count() == 0) {
            UserSystem userSystem = new UserSystem();
            userSystem.setUserName("admin");
            userSystem.setPassword("$2a$10$XURPShQNCsLjp1ESc2laoObo9QZDhxz73hJPaEv7/cBha4pk0AgP.");
            userSystem.setIsActive(Boolean.TRUE);
            userSystem.setVersion(1);
            userSystem.setCreatedOn(new Date());
            userSystem.setRoles(listRoles);
            this.userSystemRepository.save(userSystem);
        }


    }

}
