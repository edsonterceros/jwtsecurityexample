package dh.controller;

import dh.domain.UserSystem;
import dh.service.UserSystemService;
import dh.util.ApiPath;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    private UserSystemService userService;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(UserSystemService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    /**
     * This method allow to create a new user if you want other user
     *
     * @param user
     */
    @RequestMapping(value = ApiPath.USER_PATH, method = RequestMethod.POST)
    public void saveUser(@RequestBody UserSystem user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userService.saveUser(user);
    }

    @RequestMapping(value = ApiPath.USER_PATH, method = RequestMethod.GET)
    public List<UserSystem> getAllUsers() {
        return userService.getAllUsers();
    }

}
