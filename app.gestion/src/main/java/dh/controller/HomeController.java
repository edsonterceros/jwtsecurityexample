package dh.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import dh.model.Home;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class HomeController {

	private Home home = new Home("Hello World");
	
	@RequestMapping(value = "api/home", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Home> getHome() {
		return new ResponseEntity<>(home, HttpStatus.OK);
	}
}
