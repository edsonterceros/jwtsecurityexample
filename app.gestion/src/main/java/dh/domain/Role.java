package dh.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.Set;

@Entity
public class Role extends ModelBase {

    @Column(length = 200, unique = true)
    private String roleName;

    @JsonIgnore
    @ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
    private Set<UserSystem> userSystems;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Set<UserSystem> getUserSystems() {
        return userSystems;
    }

    public void setUserSystems(Set<UserSystem> userSystems) {
        this.userSystems = userSystems;
    }
}
